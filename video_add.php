<!DOCTYPE html>
<html lang="ua">
<head>
	<meta charset="UTF-8">
	<title>Security system</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/normalize.css">
	<link rel="stylesheet" href="libs/bootstrap/bootstrap-grid.min.css" />
</head>
<body>
<?php
    require_once("include/database.php");
    require_once("include/videos.php");

    $link = db_connect();
    $videos = videos_all($link);
?>
	<header class="shadow">
		<div class="logo">
			<img src="img/security.png">
		</div>
		<div class="menu">
			<ul>
				<li><a href="index.php">Головна</a></li>
		<?php
			session_start();
			if (empty($_SESSION['login']) or empty($_SESSION['id']))
    			{
			echo	'<li><a href="registration.php">Реєстрація</a></li>';
			}
			else {
				echo	'<li><a href="sugnalization.php">Сигналізація</a></li>';
                                echo	'<li><a href="admin.php">Панель адміністратора</a></li>';
			}
		?>
				<li><a href="contacts.php">Контакти</a></li>
			</ul>
		</div>
		<div class="stats">
		<?php
    		if (empty($_SESSION['login']) or empty($_SESSION['id']))
    			{
    			echo "Ви ввійшли на сайт, як гість";
    			}
    		else
    			{
    			echo "Ви ввійшли на сайт, як <span class='user-name'>".$_SESSION['login']."</span>";
    			?>
    			<form method="post" action="index.php">
	       			<input class="logout" type="submit" name="logout" value="Вийти">
             	</form>
    			<?php
    			}
			if(isset($_POST['logout'])){
			  unset($_SESSION['login']);
			  session_destroy();
			  echo "<script>javascript:window.location='index.php'</script>";
			}
		?>
		</div>
	</header>
	<div class="container">
		<div class="row">
                       <div class="main">
                              <form method="post" action="admin.php?action=add">
    <label>
      Назва
      <input class="title-add" type="text" name="title" value="" autofocus required>
    </label><br>
    <label>
      Дата
      <input class="date-add" type="date" name="date" value="" required>
    </label><br>
    <label>
      Камера
      <textarea class="video-add" name="content" required></textarea>
    </label><br>
    <input class="btn-add" type="submit" value="Зберегти" class="btn">
  </form>
			</div>
		</div>
	</div>
<script type="text/javascript" src="js/main.js"></script>
</body>
</html>
